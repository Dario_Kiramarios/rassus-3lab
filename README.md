**Uvod**

Ovaj projekt je napravljen za predmet Raspodijeljeni sustavi na FER-u.
Ovo je 3. laboratorijska vježba i rađena je po uputama sa stranica predmeta.
Cilj je bio implementirati 5 mikroservisa (temperatura, vlaga, config, eureka i agregacija) i napraviti da svi međusobno surađuju i simuliraju pravi sustav.
Nakon toga, cilj je bio sve to povezati u Dockeru i pokretati preko Dockera.


**Dodatni opis**

Prije korištenja se sve treba buildat.
Svaki mikroservis ima svoj Dockerfile i onda treba prvo u svakom folderu sa "gradlew build" (ili "./gradlew build") buildat.
Onda treba nakon toga u tom folderu napraviti image sa Dockerfileom npr "docker build -t eureka:1.3 ." (treba pripaziti da se ova točka na kraju naredbe **ne izostavi**) koji kreira image u Dockeru zvan eureka i verzija mu je 1.3

Nakon kreiranja, samo provjeri je li "dobro" imenovan taj image tj. poklapa li se sa onim koji se navodi u docker-compose-infrastructure.yml i docker-compose-services.yml.

Uvijek se prvo pokreće docker-compose-infrastructure i tek onda docker-compose-services.

GIT sa kojeg se dohvaća mjerna jedinica za temperaturu je: https://gitlab.com/Dario_Kiramarios/rassus-3lab-config.git

Sva mjerenja se učitavaju iz datoteke zvane mjerenja.csv koja je u mikroservisima od temperature i vlage kod properties dijela

AKO JAVLJA PROBLEM da config ne moze ucitati, treba samo preusmjeriti da koristi ispravni link za git profil!
(Prije je bio na githubu, a sada je na gitlabu pa treba otici u config microservice i tamo u application.properties to popraviti (preusmjeriti) i onda potencijalno rebuildati i novu sliku napraviti da sve radi kak treba)


**Korištenje programa (mikroservisa)**

Adrese za slanje GET zahtjeva:
dohvaća agregaciju temperature i vlage: 
http://localhost:8083/getAgregacija

Dohvaća trenutnu temperaturu u C: 
http://localhost:8082/currHumidity

Dohvaća trenutnu vlagu u %: 
http://localhost:8081/currTemp

Adrese za slanje POST zahtjeva:
Ako se treba azurirati mjerna jedinica za temperaturu u agregatoru (iz C u K ili obrnuto), pošalji POST zahtjev na: 
http://localhost:8083/actuator/refresh

Ostale adrese:
Link za promatranje eureke i pregled svih spojenih mikroservisa (temp, vlaga i agregacija): 
http://localhost:8761/
(ovo se otvara u pregledniku)
