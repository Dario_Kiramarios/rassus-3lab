package hr.fer.tel.agregacija;

import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@RefreshScope
@Configuration
@EnableAutoConfiguration
@Service
public class AgregacijaServis {
    @Value("${temperatura.mjernaJedinica}")
    String temperatureDefaultUnit;

    @Autowired
    private EurekaClient discoveryClient;

    public AgregiraniPodaci getCombinedData() throws IOException{
        double temperatura = getService("TEMPERATURA-SERVICE","currTemp");
        temperatura = popraviMjernuJedinicu(temperatura);
        int humidity = getService("VLAGA-SERVICE","currHumidity");

        System.out.println("Temperatura mjerna jedinica: " + temperatureDefaultUnit);

        AgregiraniPodaci combinedData = new AgregiraniPodaci(temperatura, temperatureDefaultUnit, "Temperature", humidity, "%", "Humidity");
//        AgregiraniPodaci combinedData = new AgregiraniPodaci(1, temperatureDefaultUnit, "Temperature", 1, "%", "Humidity");

        return combinedData;
    }

    private int getService(String imeServisa, String imeGetZahtjeva) throws IOException {
        String serviceUrl = discoveryClient.getNextServerFromEureka(imeServisa, false).getHomePageUrl();
        String endpointUrl = serviceUrl + imeGetZahtjeva;
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        StringBuilder response = new StringBuilder();

        try {
            URL url = new URL(endpointUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        int odgovor=extractValueFromJson(response.toString());
        return odgovor;
    }


    private double popraviMjernuJedinicu(double temperatura) {
        if (temperatureDefaultUnit.equals("K")) {
            temperatura = temperatura + 273.15;
        }
        return temperatura;
    }

    private static int  extractValueFromJson(String jsonResponse) {
        try{
            JSONObject jsonObject = new JSONObject(jsonResponse);
            return jsonObject.getInt("value");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
}
