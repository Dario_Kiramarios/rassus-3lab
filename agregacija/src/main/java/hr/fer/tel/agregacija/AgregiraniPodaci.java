package hr.fer.tel.agregacija;

public class AgregiraniPodaci {
    private double temperatura;
    private String temperaturaMjernaJedinica="C";
    private String temperaturaIme="Temperatura";

    private int humidity;
    private String vlagaMjernaJedinica ="%";
    private String vlagaIme="Humidity";


    public AgregiraniPodaci() {
    }

    public AgregiraniPodaci(double temperatura, int humidity) {
        this.temperatura = temperatura;
        this.humidity = humidity;
    }

    public AgregiraniPodaci(double temperatura, String temperaturaMjernaJedinica, String temperaturaIme, int humidity, String vlagaMjernaJedinica, String vlagaIme) {
        this.temperatura = temperatura;
        this.temperaturaMjernaJedinica = temperaturaMjernaJedinica;
        this.temperaturaIme = temperaturaIme;
        this.humidity = humidity;
        this.vlagaMjernaJedinica = vlagaMjernaJedinica;
        this.vlagaIme = vlagaIme;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getTemperaturaMjernaJedinica() {
        return temperaturaMjernaJedinica;
    }

    public String getTemperaturaIme() {
        return temperaturaIme;
    }

    public String getVlagaMjernaJedinica() {
        return vlagaMjernaJedinica;
    }

    public String getVlagaIme() {
        return vlagaIme;
    }
}
