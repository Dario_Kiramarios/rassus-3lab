package hr.fer.tel.agregacija;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class Kontroler {

    @Autowired
    AgregacijaServis AgregacijaServis;

    @GetMapping("/getAgregacija")
    private AgregiraniPodaci getAgregacija() throws IOException {
        System.out.println("UHVATIO SAM OVO");
        return AgregacijaServis.getCombinedData();
    }
}
