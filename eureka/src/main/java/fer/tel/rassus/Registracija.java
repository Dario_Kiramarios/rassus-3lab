package fer.tel.rassus;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class Registracija {
    public static void main(String[] args) {
        SpringApplication.run(Registracija.class, args);
    }
}
