package hr.fer.tel.rassus.temperatura;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


@RestController
public class Kontroler {
    private MjerenjaRepo tempRepo;
    public static long startTime=System.currentTimeMillis();

    public Kontroler(MjerenjaRepo tempRepo) {this.tempRepo=tempRepo;}

    @GetMapping("/currTemp")
    public String getCurrentTemp(){

        if (tempRepo.count()==0){
            popuniBazu();
        }

        //mora biti u minutama!
        int brojAktivnihSekundi = (int)(System.currentTimeMillis()/1000)/60;
        Integer id = (brojAktivnihSekundi % 100) + 1;

        String tempOcitanje = tempRepo.findById(id).get().toJSON();

        return tempOcitanje;
    }



    private void popuniBazu() {
        //ucitaj sve u REPO i onda kada dohvacas po formuli samo izracunas redak koji te zanima
        TempOcitanje ocitanje = new TempOcitanje();

//        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("temperatura/src/main/resources/mjerenja.csv"))) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("mjerenja.csv"))) {
            String line;
            int currentRow = 0;

            while ((line = bufferedReader.readLine()) != null) {
                if (currentRow>0) { //preskoci prvi redak
                    String[] values = line.split(",");
                    if (values[0]!=""){
                        int temp = Integer.parseInt(values[0]);
                        ocitanje.setTemp(temp);
                    }
                    else {
                        ocitanje.setTemp(0);
                    }
                    tempRepo.save(new TempOcitanje(currentRow, ocitanje.getTemp()));
                }
                currentRow++;
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }

}



