package hr.fer.tel.rassus.temperatura;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MjerenjaRepo extends JpaRepository<TempOcitanje, Integer> {
}
