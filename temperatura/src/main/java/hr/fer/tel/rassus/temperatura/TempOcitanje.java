package hr.fer.tel.rassus.temperatura;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class TempOcitanje {

    @Id
    @Column(name="id")
    private int id;

    private int temp;

    public TempOcitanje() {
    }

    public TempOcitanje(int id, int temp) {
        this.id = id;
        this.temp = temp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    @Override
    public String toString() {
        return "TempOcitanje{" +
                "ID=" + id +
                ", temp=" + temp +
                '}';
    }

    public String toJSON() {
        return  "{\"name\": \"Temperature\"," +
                "\"unit\": \"C\"," +
                "\"value\": " + temp +
                "}";
    }
}
