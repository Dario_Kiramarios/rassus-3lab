package hr.fer.tel.rassus.vlaga;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MjerenjaRepo extends JpaRepository<VlagaOcitanje, Integer> {
}
