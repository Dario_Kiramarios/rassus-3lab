package hr.fer.tel.rassus.vlaga;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@RestController
public class VlagaKontroler {

    private MjerenjaRepo humidityRepo;
    public static long startTime=System.currentTimeMillis();

    public VlagaKontroler(MjerenjaRepo tempRepo) {this.humidityRepo =tempRepo;}

    @GetMapping("/currHumidity")
    public String getCurrentHumidity(){

        if (humidityRepo.count()==0){
            popuniBazu();
        }

        //mora biti u minutama!
        int brojAktivnihSekundi = (int)(System.currentTimeMillis()/1000)/60;
        Integer id = (brojAktivnihSekundi % 100) + 1;

        String vlagaOcitanje = humidityRepo.findById(id).get().toJSON();

        return vlagaOcitanje;
    }



    private void popuniBazu() {
        //ucitaj sve u REPO i onda kada dohvacas po formuli samo izracunas redak koji te zanima
        VlagaOcitanje ocitanje = new VlagaOcitanje();

//        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("vlaga/src/main/resources/mjerenja.csv"))) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("mjerenja.csv"))) {
            String line;
            int currentRow = 0;

            while ((line = bufferedReader.readLine()) != null) {
                if (currentRow>0) { //preskoci prvi redak
                    String[] values = line.split(",");
                    if (values[2]!=""){
                        int vlaga = Integer.parseInt(values[2]);
                        ocitanje.setHumidity(vlaga);
                    }
                    else {
                        ocitanje.setHumidity(0);
                    }
                    humidityRepo.save(new VlagaOcitanje(currentRow, ocitanje.getHumidity()));
                }
                currentRow++;
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
