package hr.fer.tel.rassus.vlaga;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class VlagaOcitanje{
    @Id
    @Column(name="id")
    private int id;

    private int humidity;

    public VlagaOcitanje() {
    }

    public VlagaOcitanje(int id, int humidity) {
        this.id = id;
        this.humidity = humidity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "VlagaOcitanje{" +
                "ID=" + id +
                ", humidity=" + humidity +
                '}';
    }

    public String toJSON() {
        return  "{\"name\": \"Humidity\"," +
                "\"unit\": \"%\"," +
                "\"value\": " + humidity +
                "}";
    }
}
